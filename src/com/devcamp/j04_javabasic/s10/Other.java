package com.devcamp.j04_javabasic.s10;

public interface Other {
 void other();
 int other(int param);
 String other(String param);
}
