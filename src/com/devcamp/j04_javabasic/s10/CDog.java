package com.devcamp.j04_javabasic.s10;


public class CDog extends CPet implements IBarkable, IRunable, Other{

	@Override
	public void run() {

	}

	@Override
	public void bark() {
		System.out.println("Chos suar");

	}

	
	@Override
	public void animalSound() {
		this.bark();

	}

	@Override
	public void barkBanrk() {
		
	}

	@Override
	public void running() {
		
	}

	@Override
	public void other() {
		System.out.println("other 1");
		
	}

	@Override
	public int other(int param) {
		return 10;
	}

	@Override
	public String other(String param) {
		return "Something";
	}

}
